package main

import (
	"fmt"
	"os"
	"time"

	"github.com/dgraph-io/badger"
)

func main() {
	localDbTest()
}

func localDbTest() {
	os.Mkdir("./badger", 0777)

	options := badger.DefaultOptions
	options.Dir = "badger"
	options.ValueDir = "badger"
	db, err := badger.Open(options)
	if err != nil {
		fmt.Printf(" \n\n -------------- err: %v\n", err)
		return
	}
	defer db.Close()

	err = db.Update(func(txn *badger.Txn) error {
		fmt.Println(" ~~~~~~~~ update ~~~~~~~~~~~ ")
		var key, val []byte
		key = []byte("key1")
		val = []byte("val1")
		err := txn.SetWithTTL(key, val, 8*time.Second)
		fmt.Printf(" ============ err: %v\n", err)
		return err
	})

	err = db.View(func(txn *badger.Txn) error {
		fmt.Println(time.Now().Unix())
		fmt.Println(" ~~~~~~~~ view1111 ~~~~~~~~~~~ ")
		key := []byte("key1")
		item, err := txn.Get(key)
		if err != nil {
			fmt.Printf(" \n\n *************** err: %v\n", err)
			return err
		}
		item.Value(func(val []byte) error {
			fmt.Printf("The answer is: %s\n", val)
			return nil
		})
		fmt.Printf("item: %+v\n", item.ExpiresAt())
		return err
	})
	time.Sleep(10 * time.Second)
	err = db.View(func(txn *badger.Txn) error {
		fmt.Println(" ~~~~~~~~ view2222 ~~~~~~~~~~~ ")
		key := []byte("key1")
		item, err := txn.Get(key)
		if err != nil {
			fmt.Printf(" \n\n *************** err: %v\n", err)
			return err
		}
		item.Value(func(val []byte) error {
			fmt.Printf("The answer is: %s\n", val)
			return nil
		})
		fmt.Printf("item: %+v\n", item)
		return err
	})
}
