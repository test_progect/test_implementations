package main

import (
	"fmt"
	"time"
)

func main() {
	// var from int64 = 1568115683
	// fmt.Println(time.Unix(from, 0).Format("20060102T150405Z"))

	var inTime int64 = 1576011600 //* 1000
	etalon := time.Now().AddDate(5, 0, 0).Unix()
	fmt.Printf("etalon: %v\n value: %v\n", inTime, etalon)
	fmt.Println(CastingTimeToMiliSeconds(inTime))
	fmt.Println(inTime < etalon)
}

func CastingTimeToMiliSeconds(timestamp int64) int64 {
	// INFO: берем дату на 50 лет больше
	etalon := time.Now().AddDate(50, 0, 0).Unix()
	// INFO: если текущяя дата меньше даты через 50 лет тогда она в секундах
	// иначе умножаем на 1000
	if timestamp < etalon {
		return timestamp * 1000
	}
	return timestamp
}
