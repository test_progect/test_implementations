package main

import (
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"strings"
)

// подача нагрузки
// ab -k -c 100 -n 1000000 http://localhost:8080/test
var k int64

func sayHello(w http.ResponseWriter, r *http.Request) {
	message := r.URL.Path
	message = strings.TrimPrefix(message, "/")
	message = "Hello " + message
	w.Write([]byte(message))
	k++
	fmt.Println(" ----------- ", k)
}
func main() {
	http.HandleFunc("/", sayHello)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		panic(err)
	}
}

/*
ab -c 10 -n 100 http://localhost:8080/test
-c — количество параллельных запросов отправляемых одновременно
-n — количество отправляемых запросов
-k   Включить постоянное HTTP-соединение (KeepAlive)
-t — максимальное количество секунд отведенное на тест необходимо задать большое значение параметру -n
-C cookie-name=value — добавляем cookie в каждый запрос к серверу
-H — задаем заголовок запроса
-T — Content-type заголовок запроса
-p — файл содержащий тело POST запроса

разбор резуьтатов
Time taken for tests — суммарное время потраченное на весь тест
Complete requests — количество выполненных запросов
Failed requests — количество запросов завершенных отказом
Total transferred и HTML transferred — суммарный объем и объем html переданные во время теста
Requests per second или rps — количество обрабатываемых запросов в секунду
Time per request — среднее время затраченное на запрос с и без учета распараллеливания
Transfer rate — скорость передачи данных при прохождении теста
*/

/*
?seconds=30 время работы профайлера

профайлер cpu
go tool pprof goprofex http://127.0.0.1:8080/debug/pprof/profile

скачать профиль в файл
curl http://127.0.0.1:8080/debug/pprof/profile -o cpu.out


профайлер кучи стягивает данные в утилиту pprof
go tool pprof goprofex http://127.0.0.1:8080/debug/pprof/heap

go tool pprof -alloc_objects goprofex http://127.0.0.1:8080/debug/pprof/heap
-alloc_objects  количество размещённых в куче объектов
-inuse_objects, показывающий количество объектов в памяти;
-alloc_space, показывающий, сколько памяти было выделено с момента запуска программы.

профайлер горутин получаем стек вызова и количество работающих горутин
go tool pprof goprofex http://127.0.0.1:8080/debug/pprof/goroutine

Профайлер блокировок показывает, где в программе происходят задержки из-за блокировок
go tool pprof goprofex http://127.0.0.1:8080/debug/pprof/block

открытие профиля снятого с сервера
go tool pprof cpu.out
*/

/*
снятие трассировки файл
curl http://localhost:8080/debug/pprof/trace?seconds=2 -o trace.out

визуализация трейса
go tool trace -http "0.0.0.0:8081" ./ab_testing trace.out
*/
