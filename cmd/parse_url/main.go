package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/PuerkitoBio/goquery"
)

func main() {
	ExampleScrape()
}

func ExampleScrape() {
	// Request the HTML page.
	res, err := http.Get("https://habr.com/ru/post/268585/")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	title := doc.Find("title").Text()
	fmt.Printf("title: %v\n", title)

	// meta := doc.Find("meta").Text()
	// fmt.Printf("meta: %v\n", meta)

	doc.Find("meta").Each(func(i int, s *goquery.Selection) {
		meta := s.Find("description").Text()
		fmt.Printf("meta: %v\n", meta)
	})

}
