// синглтон патерн
package main

import (
	"fmt"
	"sync"
)

var doOnce sync.Once

func main() {
	DoSomething()
	DoSomething()
}

func DoSomething() {
	fmt.Printf(" -------------- doOnce: %#v\n", doOnce)
	doOnce.Do(func() {
		fmt.Println("Run once - first time, loading...")
	})
	fmt.Println("Run this every time")
	fmt.Printf(" -------------- doOnce: %#v\n", doOnce)
}
