package main

import "fmt"

//go:generate easyjson main.go

//easyjson:json
// ValLocalDB значение которое храниться в локальной бд для softbounce.
type ValLocalDB struct {
	attempts   uint8
	to         string
	streamType string
}

func main() {
	fmt.Println("test go generate")
}
