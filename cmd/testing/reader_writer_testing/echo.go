// пример с аодменой буфера для удобства тестирования
// заменяем буфер куда пишеться результат чтоб его было с чем сравнить

package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
)

// go run echo.go  test booling stringer

var (
	n = flag.Bool("n", false, "omit trailing newline")
	s = flag.String("s", " ", "separator")
)

var out io.Writer = os.Stdout // изминяем на случай тестирования

func main() {
	flag.Parse()
	fmt.Println("***** s: ", *s)
	fmt.Printf("---- flag: %#v\n", flag.Args())
	if err := echo(!*n, *s, flag.Args()); err != nil {
		fmt.Fprintf(os.Stderr, "echo: %v\n", err)
		os.Exit(1)
	}
}

func echo(newline bool, sep string, args []string) error {
	fmt.Fprint(out, strings.Join(args, sep))
	if newline {
		fmt.Fprintln(out)
	}
	return nil
}
