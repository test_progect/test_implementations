package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Printf("%v\n", strTrim(""))
}

func strTrim(name string) string {
	// if name[0] == '"' {
	// 	name = name[1:]
	// }
	// if name[len(name)-1] == '"' {
	// 	name = name[:len(name)-1]
	// }
	name = strings.Trim(name, `"`)
	return name
}
