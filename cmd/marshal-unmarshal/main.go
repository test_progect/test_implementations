package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	slStr := []string{"a", "b", "c"}
	bData, err := json.Marshal(slStr)
	if err != nil {
		fmt.Printf("marshal error: %v\n", err)
	}
	fmt.Printf("%s\n", bData)
}
