package main

import (
	"fmt"
	"time"
)

func main() {
	date := time.Now()
	year, month, day := date.Date()
	l := time.FixedZone("UTC+3", +3*60*60)
	dateStart := time.Date(year, month, day, 0, 0, 0, 0, l).Unix()
	dateEnd := time.Date(year, month, day, 23, 59, 59, 0, l).Unix()
	ds := time.Unix(dateStart, 0)
	de := time.Unix(dateEnd, 0)
	fmt.Printf("date: %v\ndateStart: %v\ndateEnd: %v\n", date, ds, de)
	fmt.Println("")
	dateStart = time.Date(year, month, day, 0, 0, 0, 0, time.UTC).Unix()
	dateEnd = time.Date(year, month, day, 23, 59, 59, 0, time.UTC).Unix()
	ds = time.Unix(dateStart, 0)
	de = time.Unix(dateEnd, 0)
	fmt.Printf("date: %v\ndateStart: %v\ndateEnd: %v\n", date, ds, de)
}
