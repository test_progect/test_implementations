// +build !appengine

// Эта программа демонстрирует состояние гонки.
// Чтобы наблюдать за гонкой с помощью детектора гонки:
// go build -race
// go run -race main.go
package main

import "fmt"

func main() {
	done := make(chan bool)
	m := make(map[string]string)
	m["name"] = "world"
	go func() {
		m["name"] = "data race"
		done <- true
	}()
	fmt.Println("Hello,", m["name"])
	<-done
}
