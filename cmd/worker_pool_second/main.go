package main

import (
	"fmt"
	"sync"
	"time"
)

// workerpool/task.go
type Task struct {
	Err  error
	Data interface{}
	f    func(interface{}) error
}

// NewTask созданеи задачи
func NewTask(f func(interface{}) error, data interface{}) *Task {
	return &Task{f: f, Data: data}
}

func process(workerID int, task *Task) {
	fmt.Printf("Worker %d processes task %v\n", workerID, task.Data)
	task.Err = task.f(task.Data)
}

// workerpool/worker.go

// Worker контролирует всю работу
type Worker struct {
	ID       int
	taskChan chan *Task
}

// NewWorker возвращает новый экземпляр worker-а
func NewWorker(channel chan *Task, ID int) *Worker {
	return &Worker{
		ID:       ID,
		taskChan: channel,
	}
}

// запуск worker
func (wr *Worker) Start(wg *sync.WaitGroup) {
	fmt.Printf("Starting worker %d\n", wr.ID)

	wg.Add(1)
	go func() {
		defer wg.Done()
		for task := range wr.taskChan {
			process(wr.ID, task)
		}
	}()
}

// workerpoo/pool.go

// Pool воркера
type Pool struct {
	Tasks []*Task

	concurrency int
	collector   chan *Task
	wg          sync.WaitGroup
}

// NewPool инициализирует новый пул с заданными задачами и
func NewPool(tasks []*Task, concurrency int) *Pool {
	return &Pool{
		Tasks:       tasks,
		concurrency: concurrency,
		collector:   make(chan *Task, 1000),
	}
}

// Run запускает всю работу в Pool и блокирует ее до тех пор,
// пока она не будет закончена.
func (p *Pool) Run() {
	for i := 1; i <= p.concurrency; i++ {
		worker := NewWorker(p.collector, i)
		worker.Start(&p.wg)
	}

	for i := range p.Tasks {
		p.collector <- p.Tasks[i]
	}
	close(p.collector)

	p.wg.Wait()
}

func main() {
	var allTask []*Task
	for i := 1; i <= 100; i++ {
		task := NewTask(func(data interface{}) error {
			taskID := data.(int)
			time.Sleep(100 * time.Millisecond)
			fmt.Printf("Task %d processed\n", taskID)
			return nil
		}, i)
		allTask = append(allTask, task)
	}

	pool := NewPool(allTask, 5)

	start := time.Now()

	pool.Run()

	elapsed := time.Since(start)
	fmt.Printf("Took ===============> %s\n", elapsed)
}
