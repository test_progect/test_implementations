package main

import "fmt"

type Cast struct {
	str     string
	num     int
	clise   []string
	m       map[string]string
	pointer *string
}

func main() {
	str := "pointer"
	var data Cast
	data.str = "string"
	data.num = 12
	data.clise = []string{"first", "second"}
	data.m = map[string]string{"f": "first", "s": "second"}
	data.pointer = &str

	cast(data)
	cast(Cast{})
}

func cast(in interface{}) {
	data, ok := in.(Cast)
	fmt.Printf("data %+v \n in %+v \n ok %v \n", data, in, ok)
}
