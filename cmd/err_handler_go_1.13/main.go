/*
пример новых способов обработки ошибок в go 1.13
*/

package main

import (
	"errors"
	"fmt"
)

var errStandart = errors.New("msg error")

func main() {
	err := abstract1()
	// errors.Is сравнивает ошибку со значением
	if errors.Is(err, errStandart) {
		fmt.Println("errWrap and errStandart is equal")
		fmt.Printf("errWrap: %v\nerrStandart: %v\n", err, errStandart)
	}

}

func abstract1() error {
	// fmt.Errorf есть новая команда %w.
	// Если она есть, то ошибка, возвращаемая fmt.Errorf, будет содержать метод Unwrap,
	// возвращающий аргумент %w, который должен быть ошибкой.
	// Во всех остальных случаях %w идентична %v
	return fmt.Errorf("wrap text: %w", abstract2())
}

func abstract2() error {
	return errStandart
}
