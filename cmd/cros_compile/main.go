package main

import "fmt"
import "runtime"

/*
$GOOS     $GOARCH
darwin    386      -- 32 bit MacOSX
darwin    amd64    -- 64 bit MacOSX
linux     386      -- 32 bit Linux
linux     amd64    -- 64 bit Linux
linux     arm      -- RISC Linux
windows   386      -- 32 bit Windows
windows   amd64    -- 64 bit Windows

ПР: компиляция для ОС linux х64
GOOS=linux GOARCH=amd64 go build main.go

При кросс-компиляции вы должны использовать go build, а не go install

*/

// https://www.yellowduck.be/posts/cross-compile/
// https://golang.org/doc/install/source#environment
func main() {
	fmt.Printf("OS: %s", runtime.GOOS)
	fmt.Printf("Architecture: %s", runtime.GOARCH)
}
