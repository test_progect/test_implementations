package main

import (
	"fmt"
	"strconv"
	"sync"
)

func main() {
	var testVariable string
	var wg sync.WaitGroup
	i := 0
	for ; i <= 100000; i++ {
		wg.Add(1)
		go func() {
			testVariable = strconv.Itoa(i)
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println(testVariable)
}
