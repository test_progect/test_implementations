package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
)

const SockAddr = "../echo.sock"

func fakeDial(proto, addr string) (conn net.Conn, err error) {
	return net.Dial("unix", SockAddr)
}

func main() {
	tr := &http.Transport{
		Dial: fakeDial,
	}
	client := &http.Client{Transport: tr}
	resp, err := client.Get("http://d/test")
	if err != nil {
		log.Fatal(err)
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	bodyString := string(bodyBytes)

	fmt.Printf("body:%v\n\nresp: %+v\n\nerr: %v", bodyString, resp, err)

}
