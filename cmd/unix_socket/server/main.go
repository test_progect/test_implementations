package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
)

type foo int

var count int

func (m foo) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	count++
	fmt.Fprintln(w, "Some text"+fmt.Sprint(count))
}

func main() {
	const SockAddr = "../echo.sock"
	defer os.Remove(SockAddr)

	listener, err := net.Listen("unix", SockAddr)
	if err != nil {
		log.Fatalf("Could not listen on %s: %v", SockAddr, err)
		return
	}
	defer listener.Close()
	var handlers foo
	if err = http.Serve(listener, handlers); err != nil {
		log.Fatalf("Could not start HTTP server: %v", err)
	}
}
