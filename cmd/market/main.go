package main

import "fmt"

type active struct {
	name        string
	buyPrise    float32
	curentPrise float32
	num         float32
}

func main() {
	print("project from test\n")
	portfolio := []active{
		active{name: "Роснефть", num: 430, buyPrise: 322, curentPrise: 255},
		active{name: "Газпром", num: 500, buyPrise: 170, curentPrise: 173},
		active{name: "Лукоил ", num: 34, buyPrise: 4661, curentPrise: 4230},
		active{name: "Сбер   ", num: 500, buyPrise: 200, curentPrise: 187},
	}
	down(portfolio)
}

func down(portfolio []active) {
	for _, v := range portfolio {
		pD := (v.curentPrise - v.buyPrise) / v.buyPrise * 100
		sum := (v.curentPrise - v.buyPrise) * v.num
		fmt.Printf("%s:\t%v\t%v\n", v.name, pD, sum)
	}
}

// посчет процентов для расчета кончной цены
// func procent(){
//   var sumMes float64 = 95
//   var itogo float64 = 525
//   var resProcent float64
//   for i := 0; i < 6; i++ {
//     itogo = itogo + sumMes
//     res := itogo * 4 / 100 / 12
//     resProcent += res
//   }
//   fmt.Println(resProcent * 1000)
//   fmt.Println(500 * 12 / 100)
//   fmt.Println(60 * 13 / 100)
// }
