package main

import (
	"fmt"
	"net/http"
)

func hello(w http.ResponseWriter, r *http.Request) {
	queryValues := r.URL.Query()
	fmt.Printf(" ============== %#v\n\n", queryValues)
	fmt.Fprintf(w, "hello, %s!\n", queryValues.Get("name"))
}

func main() {
	http.HandleFunc("/test", hello)

	http.ListenAndServe(":8090", nil)
}
